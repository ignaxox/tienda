<?php
/**
 * Created by PhpStorm.
 * User: ignacio
 */

App::uses('AppController', 'Controller');

class OrdersController extends AppController {

    public function index() {
        $this->layout = 'tienda';
        $this->loadModel('OrderProduct');
        //chequeamos si hay una orden pendiente
        $order = $this->Order->find('first',
            array('conditions' => array(
                "Order.user_id" => $this->Auth->user('id'),
                "Order.status" => 'pending'
            ))
        );
        if(isset($order)){
            //si existe una orden pendiente se muestran sus productos
            $productos = $this->OrderProduct->find('all',
                array('conditions' => array(
                    'order_id' => $order['Order']['id']
                )));
            $this->set('productos', $productos);
        }

        $this->set('order', $order);
    }

    public function addItem() {
        $this->loadModel('Product');
        $this->loadModel('Order');
        $this->loadModel('OrderProduct');

        $this->request->allowMethod('ajax');
        $this->autoRender = FALSE;
        $data = $this->request->data;
        $id = $data['id'];
        //obtenemos el producto que se desea añadir al carro
        $product = $this->Product->findById($id);

        if (isset($product)) {
            //Se verifica el stock
            if ($product['Product']['stock'] > 0) {
                $this->Product->id = $id;
                //Revisamos si es que el usuario tiene una compra pendiente
                $order = $this->Order->find('first',
                    array('conditions' => array(
                        "Order.user_id" => $this->Auth->user('id'),
                        "Order.status" => 'pending'
                    ))
                );
                if (!empty($order)) {
                    //si la orden existe, se añade el producto
                    $productdata = array(
                        "OrderProduct" => array(
                            "order_id"    => $order["Order"]["id"],
                            "product_id"  => $id,
                            "price"       => $product['Product']['price'],
                            "name"        => $product['Product']['name'],
                            "description" => $product['Product']['description'],
                            "image"       => $product['Product']['image']
                        )
                    );
                    $this->OrderProduct->save($productdata);

                } else {
                    //Si no existe la orden, se crea
                    $data = [];
                    $data["Order"] = array(
                        "user_id" => $this->Auth->user('id'),
                        "status"  => "pending"
                    );
                    $this->Order->save($data);
                    $idNewOrder = $this->Order->id;
                    //y luego se añade el producto
                    $productdata = array(
                        "OrderProduct" => array(
                            "order_id"    => $idNewOrder,
                            "product_id"  => $id,
                            "price"       => $product['Product']['price'],
                            "name"        => $product['Product']['name'],
                            "description" => $product['Product']['description'],
                            "image"       => $product['Product']['image']
                        )
                    );

                    $this->OrderProduct->save($productdata);
                }

                //Si se agregó al carrito se puede descontar del stock
                $this->Product->saveField('stock', ($product['Product']['stock'] - 1));

                $response = [
                    'check' => TRUE,
                    'res'   => "Producto añadido a tu carrito exitosamente"
                ];

            } else {
                $response = [
                    'check' => FALSE,
                    'res'   => "Producto sin stock"
                ];
            }

        } else {
            $response = [
                'check' => FALSE,
                'res'   => "No hemos podido encontrar el producto"
            ];
        }

        return json_encode($response);
    }

    public function removeItem() {
        $this->loadModel('OrderProduct');
        $this->request->allowMethod('ajax');
        $this->autoRender = FALSE;
        $data = $this->request->data;
        $id = $data['id'];
        //eliminamos el producto del carrito
        $this->OrderProduct->delete($id);
        $response = [
            'check' => TRUE,
            'res'   => "Producto elminado de tu carrito exitosamente"
        ];

        return json_encode($response);

    }

    public function checkout() {
        $this->layout = 'tienda';
        $this->loadModel('OrderProduct');
        //chequeamos si hay una orden pendiente
        $order = $this->Order->find('first',
            array('conditions' => array(
                "Order.user_id" => $this->Auth->user('id'),
                "Order.status" => 'pending'
            ))
        );
        $productos = $this->OrderProduct->find('all',
            array('conditions' => array(
                'order_id' => $order['Order']['id']
            )));

        $this->set('order', $order);
        $this->set('productos', $productos);
    }

    public function sendCheckout($id = NULL) {
        $this->autoRender = FALSE;

        $this->Order->id = $id;

        if (!$this->Order->exists()) {
            throw new NotFoundException(__('Invalid order'));
        } else {
            $data = $this->request->data;
            //cambiamos el status del pedido a validado para finalizar
            $data['Order']['status'] = 'validado';
            if ($this->Order->save($data)) {
                $this->Session->setFlash(__('Tu pedido ha sido validado y será despachado en los próximos días'));

                return $this->redirect(array('action' => 'success'));
            }
        }

    }

    public function success() {
        //Mostramos la página de éxito
        $this->layout = 'tienda';

    }
}