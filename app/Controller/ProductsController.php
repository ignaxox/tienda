<?php
/**
 * Created by PhpStorm.
 * User: ignacio
 */
App::uses('AppController', 'Controller');

class ProductsController extends AppController {

    public function index() {
        $this->layout = 'tienda';
        $products = $this->Product->find('all');
        $this->set('products', $products);
    }

    public function view($id) {
        $this->layout = 'tienda';
        $product = $this->Product->findById($id);
        $this->set('product', $product);
    }


}