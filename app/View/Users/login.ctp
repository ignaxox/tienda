<div class="container">
    <div class="row">
        <div class="col-lg-6 offset-3">
            <form action="/tienda/users/login" id="UserLoginForm" method="post" accept-charset="utf-8">
                <div style="display:none;">
                    <input type="hidden" name="_method" value="POST"></div>
                <fieldset>
                    <legend>Ingresa un nombre de usuario y contraseña</legend>
                    <div class="input text required">
                        <label for="UserUsername">Usuario</label>
                        <input name="data[User][username]" maxlength="255" type="text" id="UserUsername" required="required">
                    </div>
                    <div class="input password required">
                        <label for="UserPassword">Contraseña</label>
                        <input name="data[User][password]" type="password" id="UserPassword" required="required">
                    </div>
                </fieldset>
                <div class="submit"><input type="submit" value="Login"></div>
            </form>
        </div>
    </div>
</div>