<div class="content">
    <div class="row">
        <div class="col-lg-6 offset-3">
            <form action="/tienda/users/add" id="UserAddForm" method="post" accept-charset="utf-8">
                <div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
                <fieldset>
                    <legend>Crear Usuario</legend>
                    <div class="input text required">
                        <label for="UserUsername">Username</label>
                        <input name="data[User][username]" type="text" id="UserUsername" required="required">
                    </div>
                    <div class="input password required">
                        <label for="UserPassword">Password</label>
                        <input name="data[User][password]" type="password" id="UserPassword" required="required">
                    </div>
                </fieldset>
                <div class="submit"><input type="submit" value="Submit"></div>
            </form>
        </div>
    </div>
</div>