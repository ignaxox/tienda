<div class="card">
    <div class="card-body">
        <h1>Nuestros productos</h1>

        <?php
        $numprod = count($products);
        if ($numprod > 0) {
            foreach ($products as $key => $product) {
                $product = $product['Product'];
                if ($key % 4 == 0) {
                    echo '<div class="row">';
                }
                echo '<div class="col-lg-3">
                        <div class="card">
                            <img src="' . $product['image'] . '" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">' . $product['name'] . '</h5>
                                <p class="card-text">' . $product['description'] . '.</p>
                                <a href="/tienda/products/view/' . $product['id'] . '" class="btn btn-primary">Ver</a>
                            </div>
                        </div>
                        </div>';
                if (($key + 1) % 4 == 0 || ($key + 1) == $numprod) {
                    echo '</div>';
                }

            }
        } else {
            echo '<h1>Aún no hay productos disponibles</h1>';
        }

        ?>
    </div>
</div>
</div>