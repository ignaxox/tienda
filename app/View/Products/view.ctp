<?php $product = $product['Product']; ?>
<div class="row">
    <div class="col-lg-8 offset-lg-2">
        <div class="jumbotron">
            <div class="row">
                <div class="col-lg-4">
                    <h1 class="display-4"><?php echo $product['name']; ?></h1>
                    <p class="lead"><?php echo $product['description']; ?></p>
                    <dl>
                        <dt>Precio</dt>
                        <dd>$<?php echo $product['price']; ?></dd>
                        <dt>Cantidad disponible</dt>
                        <dd><?php echo $product['stock']; ?></dd>
                    </dl>
                    <button id="anadircarrito" data-id="<?php echo $product['id'];?>" class="btn btn-primary" <?php if($product['stock'] == 0){echo 'disabled';} ?>>
                        Añadir al carrito  <i class="fas fa-shopping-cart"></i>
                    </button>
                </div>
                <div class="col-lg-8">
                    <img src="<?php echo $product['image']; ?>" class="img-fluid" alt="...">
                </div>
            </div>
        </div>
    </div>
</div>

