<?php
if (!empty($order) && !empty($productos)) {
    ?>

    <div class="row mt-md-3">
        <div class="col-lg-6 offset-lg-1">
            <h2>Mis productos seleccionados</h2>
            <?php
            foreach ($productos as $p) {
                $producto = $p["OrderProduct"];
                echo '<div class="media">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="' . $producto["image"] . '" class="img-thumbnail img-fluid align-self-start mr-3" alt="" >
                            </div>
                            <div class="col-lg-8">
                            <div class="media-body">
                                <h5 class="mt-0">' . $producto["name"] . '</h5>
                                <p>' . $producto["description"] . '</p>
                                <p>$' . $producto["price"] . '</p>
                                <p><button id="removercarrito" class="btn btn-danger btn-sm" data-id="' . $producto["id"] . '">Quitar de mi carrito</button></p>
                              </div>
                            </div>
                        </div>
                    </div>';

            }
            ?>
        </div>
        <div class="col-lg-4">
            <div class="jumbotron ">
                <div class="container">
                    <h1 class="display-4">Tu carrito</h1>
                    <p class="lead">Este es el detalle de tu compra.</p>
                    <p>N° productos: <?php echo count($productos);?></p>
                    <p>Total: $ <?php $total = 0; foreach ($productos as $producto){ $total = $total + $producto["OrderProduct"]["price"]; } echo $total;?></p>
                    <p><a href="/tienda/orders/checkout"><button class="btn btn-primary">Checkout</button></a></p>
                </div>
            </div>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <h3>Aún no has seleccionado ningún producto</h3>
        </div>
    </div>
    <?php
}
?>