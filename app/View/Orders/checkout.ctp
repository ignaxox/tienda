<div class="row mt-md-3">
    <div class="col-lg-6 offset-lg-1">
        <h3>Completa la información antes de proceder al pago</h3>
        <form class="needs-validation" action="/tienda/orders/sendCheckout/<?php echo $order['Order']['id'];?>" method="post" accept-charset="utf-8" novalidate>
            <div class="form-group">
                <label for="first_name">Nombre</label>
                <input type="text" class="form-control" id="first_name" name="data[Order][first_name]" placeholder="Nombre" required>
            </div>
            <div class="form-group">
                <label for="last_name">Apellido</label>
                <input type="text" class="form-control" id="last_name" name="data[Order][last_name]" placeholder="Apellido" required>
            </div>
            <div class="form-group">
                <label for="phone">Teléfono de contacto</label>
                <input type="text" class="form-control" id="phone" name="data[Order][phone]" placeholder="(+56) 9 xxxx xxxx" required>
            </div>
            <div class="form-group">
                <label for="billing_address">Dirección de facturación</label>
                <input type="text" class="form-control" id="billing_address" name="data[Order][billing_address]" placeholder="Dirección" required>
            </div>
            <div class="form-group">
                <label for="shipping_address">Dirección de despacho</label>
                <input type="text" class="form-control" id="shipping_address" name="data[Order][shipping_address]" placeholder="Dirección" required>
            </div>
            <div class="form-group">
                <label for="shipping_method">Método de despacho</label>
                <select id="shipping_method" class="form-control" name="data[Order][shipping_method]" >
                    <option value="A domicilio">A domicilio</option>
                    <option value="Retiro en local">Retiro en local</option>
                </select>
            </div>
            <div class="form-group">
                <label for="payment_method">Método de pago</label>
                <select id="payment_method" class="form-control" name="data[Order][payment_method]" >
                    <option value="Tarjeta de credito">Tarjeta de crédito</option>
                    <option value="Tarjeta de debito">Tarjeta de débito</option>
                    <option value="Transferencia">Transferencia</option>
                </select>
            </div>
            <div class="form-group">
                <label for="comment">Notas adicionales</label>
                <textarea class="form-control" id="comment" rows="3" required name="data[Order][comment]" ></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Proceder al pago</button>
        </form>
    </div>
    <div class="col-lg-4">
        <div class="jumbotron ">
            <div class="container">
                <h1 class="display-4">Tu carrito</h1>
                <p class="lead">Este es el detalle de tu compra.</p>
                <p>N° productos: <?php echo count($productos); ?></p>
                <p>Total: $ <?php $total = 0;
                    foreach ($productos as $producto) {
                        $total = $total + $producto["OrderProduct"]["price"];
                    }
                    echo $total; ?></p>
            </div>
        </div>
    </div>
</div>