DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `modified` (`modified`)
);

insert into products(name, description, stock, image, price, created, modified)
values('Diet Water', 'Agua baja en calorias', 10, 'https://www.teamjimmyjoe.com/wp-content/uploads/2013/06/Diet-Water.jpg', 2000, now(),now());
insert into products(name, description, stock, image, price, created, modified)
values('Regador', 'Autoregador de plantas', 20, 'https://plainmagazine.com/wp-content/uploads/2014/04/15-useless-product-designs11.png', 3000, now(),now());
insert into products(name, description, stock, image, price, created, modified)
values('Tazon de comida seca', 'No usar para sopa', 25, 'https://allthatsinteresting.com/wordpress/wp-content/uploads/2014/09/uselessobjectsbowl.jpg', 500, now(),now());
insert into products(name, description, stock, image, price, created, modified)
values('Pack de tazones', 'Para quienes desayunan en pareja', 35, 'https://www.demilked.com/magazine/wp-content/uploads/2017/09/59cde1e6af6ce-21_double_mugs-59ca1c572a04e__700.jpg', 500, now(),now());
insert into products(name, description, stock, image, price, created, modified)
values('Tenedor', 'Para comida de mucho grosor', 5, 'https://allthatsinteresting.com/wordpress/wp-content/uploads/2014/09/uselessobjectsfork.jpg', 100, now(),now());
insert into products(name, description, stock, image, price, created, modified)
values('Porta vela', 'Ideal para peregrinaciones', 0, 'https://twistedsifter.files.wordpress.com/2019/04/guy-designs-funny-useless-products-to-solve-problems-that-dont-exist-1.jpg?w=800&h=800', 2500, now(),now());
insert into products(name, description, stock, image, price, created, modified)
values('Lentes de sol', 'Para esos dias soleados', 0,'https://twistedsifter.files.wordpress.com/2019/04/guy-designs-funny-useless-products-to-solve-problems-that-dont-exist-5.jpg?w=800&h=800', 2000, now(),now());

--

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `order_products`;

CREATE TABLE `order_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` decimal(8,2) unsigned DEFAULT NULL,
  `shipping_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);