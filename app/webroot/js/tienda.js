$(document).ready(function () {

    $('body').on('click', '#anadircarrito', function () {

        var id = $(this).data('id');
        $.ajax({
            url: '/tienda/Orders/addItem',
            dataType: 'json',
            type: 'POST',
            data: {
                'id': id,
            },
            success: function (data) {
                alert(data.res);
                window.location.reload();
            }
        });
    });
    $('body').on('click', '#removercarrito', function () {

        var id = $(this).data('id');
        $.ajax({
            url: '/tienda/Orders/removeItem',
            dataType: 'json',
            type: 'POST',
            data: {
                'id': id,
            },
            success: function (data) {
                alert(data.res);
                window.location.reload();
            }
        });
    });

    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
});