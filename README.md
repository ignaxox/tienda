# Carrito de compra en CakePHP2


A continuación se encontrarán las instrucciones para ver el carrito


Crea una carpeta en el directorio raíz de apache

```sh
$ cd /to/apache/document/root
$ mkdir tienda
```

A continuación, clona el proyecto en la carpeta recién creada

```sh
$ git clone https://ignaxox@bitbucket.org/ignaxox/tienda.git tienda
```

Configura los permisos necesarios para poder acceder al contenido, específicamente los permisos para la carpeta tmp

```sh
$ chmod g+w -R tienda/app/tmp
```

Crea una tabla en mysql con el nombre que tu quieras, posteriormente ejecuta el script que contiene las tablas y datos iniciales

```sh
$ mysql -u tusuario -p bdcreada < tienda/app/Config/Schema/tienda.sql
```

Ahora debemos configurar la conexión a la base de datos en el proyecto, para esto, copia o renombra el archivo database.php.default, ubicado en tienda/app/Config para que quede así

```sh
 database.php
```

Ingresa al archivo y modifica los campos necesarios para realizar la conexión.

Si has configurado todo bien, podrás ver el contenido del sitio en:

```sh
 http://localhost/tienda/products
```

Al ingresar por primera vez, serás redireccionado a un login, puedes ingresar con las siguientes credenciales:

```sh
usuario: test
contraseña: test
```

Si quieres crear tu propio usuario, puedes hacerlo aquí

```sh
 http://localhost/tienda/users/add
```
