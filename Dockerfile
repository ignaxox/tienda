FROM php:7.1-apache
MAINTAINER ignacio arredondo <jo.arredondo@gmail.com>

WORKDIR /var/www/html

COPY ./default.conf /etc/apache2/sites-available/000-default.conf

RUN docker-php-ext-install pdo pdo_mysql

CMD apt-get update && \
  apt-get -y install curl git libicu-dev libpq-dev zlib1g-dev zip && \
  docker-php-ext-install intl mbstring pcntl pdo_mysql pdo_pgsql zip && \
  mkdir /var/www/html/tienda && \
  chown -R www-data:www-data /var/www/html && \
  chmod -R 750 /var/www/html && \
  curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
  a2enmod rewrite &&  \
  service apache2 restart

COPY --chown=www-data:www-data . /var/www/html/tienda
